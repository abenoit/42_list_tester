# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: abenoit <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/09/23 18:47:34 by abenoit           #+#    #+#              #
#    Updated: 2021/01/19 16:13:04 by abenoit          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

OS = $(shell uname -s)

ifeq ($(OS),Darwin)
	OS_DEF = -D os_mac
else
	OS_DEF = -D os_linux
endif

CC = clang

C_SRC += utils.c
C_SRC += rec_gnl.c

C_SRC += call_list_test.c
C_SRC += list_utils.c

C_OBJ = $(C_SRC:.c=.o)

CFLAGS = -Wall -Werror -Wextra

ifeq ($(d), 1)
	CFLAGS += -g -fsanitize=address
endif

LIB_LST_DIR = liblst/

INC = -I $(LIB_LST_DIR)inc

INC_LIB += -lc
INC_LIB += -L$(LIB_LST_DIR) -llst

BIN_NAME = test_list

RM = rm -f

SRC_DIR = src

ifneq ("$(wildcard ./$(SRC_DIR)/ft_lstnew.c)","")
SRC_NAME += ft_lstnew.c
endif
ifneq ("$(wildcard ./$(SRC_DIR)/ft_lstadd_front.c)","")
SRC_NAME += ft_lstadd_front.c
endif
ifneq ("$(wildcard ./$(SRC_DIR)/ft_lstsize.c)","")
SRC_NAME += ft_lstsize.c
endif
ifneq ("$(wildcard ./$(SRC_DIR)/ft_lstlast.c)","")
SRC_NAME += ft_lstlast.c
endif
ifneq ("$(wildcard ./$(SRC_DIR)/ft_lstadd_back.c)","")
SRC_NAME += ft_lstadd_back.c
endif
ifneq ("$(wildcard ./$(SRC_DIR)/ft_lstdelone.c)","")
SRC_NAME += ft_lstdelone.c
endif
ifneq ("$(wildcard ./$(SRC_DIR)/ft_lstclear.c)","")
SRC_NAME += ft_lstclear.c
endif
ifneq ("$(wildcard ./$(SRC_DIR)/ft_lstiter.c)","")
SRC_NAME += ft_lstiter.c
endif
ifneq ("$(wildcard ./$(SRC_DIR)/ft_lstmap.c)","")
SRC_NAME += ft_lstmap.c
endif

SRC = $(addprefix $(SRC_DIR)/,$(SRC_NAME))

OBJ_DIR = obj

OBJ_NAME = $(SRC_NAME:.c=.o)

OBJ = $(addprefix $(OBJ_DIR)/,$(OBJ_NAME))

all: print_src $(BIN_NAME)

print_src:
ifneq ("$(wildcard ./$(SRC_DIR)/ft_lstnew.c)","")
	@echo "\033[32m" ft_lstnew.c "\033[0m"
else
	@echo "\033[31m" ft_lstnew.c "\033[0m"
endif
ifneq ("$(wildcard ./$(SRC_DIR)/ft_lstadd_front.c)","")
	@echo "\033[32m" ft_lstadd_front.c "\033[0m"
else
	@echo "\033[31m" ft_lstadd_front.c "\033[0m"
endif
ifneq ("$(wildcard ./$(SRC_DIR)/ft_lstsize.c)","")
	@echo "\033[32m" ft_lstsize.c "\033[0m"
else
	@echo "\033[31m" ft_lstsize.c "\033[0m"
endif
ifneq ("$(wildcard ./$(SRC_DIR)/ft_lstlast.c)","")
	@echo "\033[32m" ft_lstlast.c "\033[0m"
else
	@echo "\033[31m" ft_lstlast.c "\033[0m"
endif
ifneq ("$(wildcard ./$(SRC_DIR)/ft_lstadd_back.c)","")
	@echo "\033[32m" ft_lstadd_back.c "\033[0m"
else
	@echo "\033[31m" ft_lstadd_back.c "\033[0m"
endif
ifneq ("$(wildcard ./$(SRC_DIR)/ft_lstdelone.c)","")
	@echo "\033[32m" ft_lstdelone.c "\033[0m"
else
	@echo "\033[31m" ft_lstdelone.c "\033[0m"
endif
ifneq ("$(wildcard ./$(SRC_DIR)/ft_lstclear.c)","")
	@echo "\033[32m" ft_lstclear.c "\033[0m"
else
	@echo "\033[31m" ft_lstclear.c "\033[0m"
endif
ifneq ("$(wildcard ./$(SRC_DIR)/ft_lstiter.c)","")
	@echo "\033[32m" ft_lstiter.c "\033[0m"
else
	@echo "\033[31m" ft_lstiter.c "\033[0m"
endif
ifneq ("$(wildcard ./$(SRC_DIR)/ft_lstmap.c)","")
	@echo "\033[32m" ft_lstmap.c "\033[0m"
else
	@echo "\033[31m" ft_lstmap.c "\033[0m"
endif

$(BIN_NAME): $(C_OBJ) $(OBJ) LIB_LST
	$(CC) $(CFLAGS) $(C_OBJ) $(OBJ) -o $(BIN_NAME) $(INC) $(INC_LIB)

%.o: %.c
	$(CC) $(INC) $(BONUS_DEF) -c $< -o $@

$(OBJ_DIR)/%.o : $(SRC_DIR)/%.c | $(OBJ_DIR)
	    $(CC) $(INC) $(CFLAGS) $(MLX_DEF) -c $< -o $@

$(OBJ_DIR):
		mkdir -p $@

LIB_LST:
		make -C $(LIB_LST_DIR)

clean:
	$(RM) -r $(OBJ_DIR)
	$(RM) $(C_OBJ)
	$(RM) $(C_OBJ_BONUS)

fclean: clean
	$(RM) $(BIN_NAME)
	make -C $(LIB_LST_DIR) fclean

re: fclean all

.PHONY: clean fclean re
