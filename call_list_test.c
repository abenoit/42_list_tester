#include "lib_lst.h"
#include "list_test.h"

int	call_add_front(int *state, t_list **lst)
{
	int		*val;

	ft_putstr("void\t\tft_lst\033[4madd_front\033[0m(t_list **alst, t_list *new)\n");
	ft_putstr("===New Elem===\n");
	ft_putstr("val = ");
	val = malloc(sizeof(int));
	*val = get_num(state);
	ft_lstadd_front(lst, ft_lstnew(val));
	*state = 0;
	return (0);
}

int	call_size(int *state, t_list **lst)
{
	int	size;

	ft_putstr("int\t\tft_lst\033[4msize\033[0m(t_list *lst)\n");
	size = ft_lstsize(*lst);
	printf("size = %d\n", size);
	*state = 0;
	return (0);
}

int	call_last(int *state, t_list **lst)
{
	t_list	*ptr;

	ft_putstr("t_list\t*ft_lst\033[4mlast\033[0m(t_list *lst)\n");
	ptr = ft_lstlast(*lst);
	print_lst(ptr);
	*state = 0;
	return (0);
}

int	call_add_back(int *state, t_list **lst)
{
	int		*val;

	ft_putstr("void\t\tft_lst\033[4madd_back\033[0m(t_list **alst, t_list *new)\n");
	ft_putstr("===New Elem===\n");
	ft_putstr("val = ");
	val = malloc(sizeof(int));
	*val = get_num(state);
	ft_lstadd_back(lst, ft_lstnew(val));
	*state = 0;
	return (0);
}

int	call_delone(int *state, t_list **lst)
{
	int		val;
	t_list	*ptr;
	t_list	*prev;

	ft_putstr("void\t\tft_lst\033[4mdelone\033[0m(t_list *lst, void(*del)(void*))\n");
	ft_putstr("===Del one===\n");
	ft_putstr("destroy elem # = ");
	val = get_num(state);
	if ((*lst)->next == NULL)
	{
		ft_lstdelone(*lst, free);
		*lst = NULL;
	}
	else
	{
		prev = *lst;
		ptr = prev->next;
		if (val == 0)
		{
			ft_lstdelone(prev, free);
			prev = NULL;
			*lst = ptr;
		}
		else
		{
			while (ptr->next != NULL && val > 1)
			{
				prev = ptr;
				ptr = prev->next;
				val--;
			}
			prev->next = ptr->next;
			ft_lstdelone(ptr, free);
		}
	}
	*state = 0;
	return (0);
}

int	call_clear(int *state, t_list **lst)
{
	ft_putstr("void\t\tft_lst\033[4mclear\033[0m(t_list **lst, void (*del)(void*))\n");
	ft_lstclear(lst, free);
	*state = 0;
	return (0);
}

int	call_iter(int *state, t_list **lst)
{
	ft_putstr("void\t\tft_lst\033[4miter\033[0m(t_list *lst, void (*f)(void *))\n");
	ft_lstiter(*lst, ft_add);
	*state = 0;
	return (0);
}

int	call_map(int *state, t_list **lst)
{
	t_list	*ptr;

	ft_putstr("t_list\t*ft_lst\033[4mmap\033[0m(t_list *lst, void *(*f)(void *),");
	ft_putstr(						"void (*del)(void *))\n");
	ptr = ft_lstmap(*lst, ft_yolo, free);
	if (ptr == NULL)
		return (0);
	ft_lstclear(lst, free);
	*lst = ptr;
	*state = 0;
	return (0);
}

int	call_print(int *state, t_list **lst)
{
	print_lst(*lst);
	*state = 0;
	return (0);
}

static int	get_list_input(int *state, t_list **lst)
{
	char	*line;

	ft_putstr("\n");
	ft_putstr("\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\n");
	ft_putstr("\"\"\"\"\"\"\"\" List tester \"\"\"\"\"\"\"\"\n");
	ft_putstr("\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\n");
	ft_putstr("1.add_front\n");
	ft_putstr("2.size\n");
	ft_putstr("3.last\n");
	ft_putstr("4.add_back\n");
	ft_putstr("5.delone\n");
	ft_putstr("6.clear\n");
	ft_putstr("7.iter\n");
	ft_putstr("8.map\n");
	ft_putstr("9.print\n");
	ft_putstr("10.exit\n");
	rec_gnl(0, &line);
	if (ft_strcmp(line, "exit") == 0 || ft_strcmp(line, "10") == 0
		|| ft_strcmp(line, "..") == 0)
		*state = -1;
	else if (ft_strcmp(line, "add_front") == 0 || ft_strcmp(line, "1") == 0)
		*state = 1;
	else if (ft_strcmp(line, "size") == 0 || ft_strcmp(line, "2") == 0)
		*state = 2;
	else if (ft_strcmp(line, "last") == 0 || ft_strcmp(line, "3") == 0)
		*state = 3;
	else if (ft_strcmp(line, "add_back") == 0 || ft_strcmp(line, "4") == 0)
		*state = 4;
	else if (ft_strcmp(line, "delone") == 0 || ft_strcmp(line, "5") == 0)
		*state = 5;
	else if (ft_strcmp(line, "clear") == 0 || ft_strcmp(line, "6") == 0)
		*state = 6;
	else if (ft_strcmp(line, "iter") == 0 || ft_strcmp(line, "7") == 0)
		*state = 7;
	else if (ft_strcmp(line, "map") == 0 || ft_strcmp(line, "8") == 0)
		*state = 8;
	else if (ft_strcmp(line, "print") == 0 || ft_strcmp(line, "9") == 0)
		*state = 9;
	else
		return (-1);
	free(line);
	return (0);
}

int	main(int ac, char **av)
{
	int		state;
	t_list	*lst;
	const	t_list_func	fsm[] = {get_list_input, call_add_front, call_size, call_last, call_add_back, call_delone, call_clear, call_iter, call_map, call_print};

	state = 0;
	lst = NULL;
	(void)av;
	if (ac != 1)
	{
		printf("Error: argument\n");
		return (-1);
	}
	while (state != -1)
	{
		if (fsm[state](&state, &lst) < 0)
			printf("Error: input\n");
	}
	ft_lstclear(&lst, free);
	return (0);
}
