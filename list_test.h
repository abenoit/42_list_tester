#ifndef LIST_TEST_H
# define LIST_TEST_H

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

#define _XOPEN_SOURCE

typedef int	(*t_func)(int *state);

# ifndef T_LIST_FORMAT
#  define T_LIST_FORMAT

typedef struct	s_list
{
	void			*data;
	struct s_list	*next;
}				t_list;

# endif


typedef int	(*t_list_func)(int *state, t_list **lst);

/*
**	test functions
*/

int		list_test(int size);
int		call_list_test(int *state);

/*
**	list utils
*/

int		ft_int_comp(void *n1, void *n2);
void	ft_add(void *content);
void	*ft_yolo(void *content);
int		print_lst(t_list *first);
int		*new_fixed_num(int val);
t_list	*new_fixed_elem(int val);
int		*new_rand_num(void);
t_list	*new_rand_elem(void);

/*
**	util functions
*/

void	ft_putchar(char c);
void	ft_putstr(char *str);
int		ft_isset(char c, char *set);
int		ft_atoi(char *str);
int		ft_strcmp(const char *s1, const char *s2);
char	*ft_strdup(const char *s1);
int		get_str(int *state, char **str);
int		get_num(int *state);
int		get_fd(int *state, int mode);


/*
**	test functions
*/

int		rec_gnl(int fd, char **line);
int		full_test(void);

#endif
