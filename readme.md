An interactive tester for the basic linked list functions of 42 libft: <br>
https://framagit.org/abenoit/libft

Build the project with an empty src/ folder in order to try normal behaviour.<br>
When you're ready, create or import the .c files for the functions you want to test in the src folder and build the project again.<br>
If your file was detected, it will appear green during compilation.
